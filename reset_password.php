<?php
require 'app/init.php';
if(!empty($_GET['t']))
{
    $token = $_GET['t'];
    if($token && $tokenHandler->isValid($token,0)):
?>
<form action="reset_password.php" method="POST">
    <fieldset>
        <legend>Reset Password</legend>
        <label>
            New Password:
            <input type="password" name ="password">
        </label>
        <br><br>
        <input type="hidden" name="t" value="<?= $token; ?>">
        <input type="submit" value="Reset Password" name="submit">
    </fieldset>
</form>
<?php
    else:
        echo "<h3>Invalid Link, please try with authenticated Link!<h3>";
    endif;
}
    // when i submit the form
else if(!empty($_POST))
{
    $password = $_POST['password'];
    $token = $_POST['t'];
    if($tokenHandler->isValid($token, 0))
    {
        $user = $userHelper->findUserByToken($token);
        if($auth->updatePassword($user->id, $password))
        {
            $tokenHandler->deleteTokenByToken($token);
            echo "<h3>Password Reset Successfully!</h3>";
            echo "<br><a href='signin.php'>Sign In</a>";
        }
    else
        {
            echo "Problem with server while updating password , please try again later!";
        }
    }
    else
    { 
        echo "your timeout,please try to generate a new link!";

    }  
}
else
{
    echo "<h3>This looks like some fishy activity, we will report it to admin</h3>";
}