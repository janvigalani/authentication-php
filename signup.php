<?php
require_once 'app/init.php';

if(!empty($_POST))
{
    
    $validator->check($_POST,[
        'email' => [
            'required'=>true,
            'maxlength' =>200,
            'email'=>true,
            'unique'=>'users'
        ],
        'username' => [
            'required'=>true,
            'maxlength' =>20,
            'minlength'=> 3,
            'unique'=>'users'
        ],
        'password' => [
            'required'=>true,
            'maxlength' => 8,
            'maxlength'=>255
        ]
    ]);
    if($validator->fails()){
        print_r($validator->errors()->all());
    } else {
        $email = $_POST['email'];
        $username = $_POST['username'];
        $password = $_POST['password'];

        $created = $auth->create([
            'email'=> $email,
            'username'=> $username,
            'password'=> $password
        ]);

        if($created) {
            header("Location: index.php");

        } else {
            echo "There was some issue while creating your user !";
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <h1>Sign Up</h1>
    <form action="signup.php" method = "POST">
        <fieldset>
            <legend>Sign Up</legend>
            <label>Email 
                <input type="email" name = "email">
                <?php
                    if($validator->fails() && $validator->errors()->has('email')){
                        echo $validator->errors()->first('email');
                    }
                ?>
            </label>
            <br>
            <br>
            <label>Username
                <input type="text" name = "username">
            </label>
            <br>
            <br>
            <label>Password
                <input type="password" name = "password">
            </label>
            <br>
            <br>
            <input type="submit" value = "Sign Up">
        </fieldset>
    </form>
    
</body>
</html>