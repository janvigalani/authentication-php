<?php
require('app/init.php');
if(!empty($_POST))
{
    $email = $_POST['email'];
    $user = $userHelper->findUserByEmail($email);
    if($user)
    {
        $tokenData = $tokenHandler->createForgetPasswordToken($user->id);
        
        if($tokenData)
        {
            $mail->addAddress($user->email);
            $mail->Subject = "Password Recovery!!";
            $mail->Body = "Use this link within 10 min to reset the password: <br>";
            $mail->Body = "<a href = 'http://localhost:3000/reset_password.php?t={$tokenData['token']}'>Reset Password</a>";
            if($mail->send())
            {
                echo"Please check your email to reset your password!";
            }
            else
            {
                echo "Problem sending mail, please try again later!";
            }
        }
        else
        {
            echo "<h3>There is some issue in server, please try again later!";
        }
    }
    else
    {
        echo "<h3>No such email id found!";
    }
}
else if($auth->check())
{
    echo "<h3>You are already signed in , how can u forget the password</h3>";
}
else
{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forgot Password</title>
</head>
<body>
    <h1>Forgot Password</h1>
    <form action="forgot_password.php" method="POST">
        <fieldset>
            <legend>Forgot Password</legend>
            <label>
                Email: <br>
                <input type="email" name = "email">
            </label>
            <br><br>
            <input type="submit" value="Reset Password" name = "submit">
        </fieldset>
    </form>
</body>
</html>

<?php
 
}

?>