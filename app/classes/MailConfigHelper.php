<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

class MailConfigHelper
{
    public static function getMailer(int $debugMode=0): PHPMailer
    {

    $mail = new PHPMailer();

    $mail->SMTPDebug = $debugMode;
    $mail->isSMTP();
    $mail->Host = 'smtp.mailtrap.io';
    $mail->Port = 2525;
    $mail->SMTPAuth = true;
    $mail->Username = '65e93ecf7b1413';
    $mail->Password = '0a61366440f4a6';
    $mail->SMTPSecure = 'tls';
    $mail->isHTML(true);

    $mail->setFrom('admin@studylinkclasses.com','Admin<SL Team>');
    return $mail;
    
    }
}