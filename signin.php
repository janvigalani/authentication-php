<?php
require "app/init.php";

if(isset($_POST['submit']))
{
    $username = $_POST['username'];
    $password = $_POST['password'];
    $rememberMe = $_POST['rem'];
    $status = $auth->signin($username, $password);
    if($status)
    {
        if($rememberMe)
        {
            $user = $userHelper->findUserByUsername($username);
            $token = $tokenHandler->createRememberMeToken($user->id);
            setcookie("token",$token['token'],time()+1800);
        }
        header('Location: index.php');
    }
    else
    {
        echo"wrong username/password";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
</head>
<body>
<?php if(isset($_COOKIE['token']) && $tokenHandler->isValid($_COOKIE['token'],1)): ?>
<h3>You are already signed in </h3>
<?php else: ?>
    <h1>Login</h1>
    <form action="signin.php" method="POST">
        <fieldset>
            <legend>Sign In</legend>
            <label>
                Username:
                <input type="text" name="username">
            </label>
            <br>
            <br>
            <label>
                Password:
                <input type="password" name="password">
            </label>
            <br>
            <br>
            <label>
                <input type="checkbox" name="rem" checked>Remember Me
            </label>
            <br>
            <br>
            <input type="submit" name="submit" value="Sign In">
         </fieldset>
    </form>
<?php endif; ?>
</body>
</html>